//Author: Rhiannon

//BEGIN

FetchPageFromCookie();

//FETCH PAGE FROM COOKIE

function FetchPageFromCookie() 
{
	var page = getCookie("page");

	if (page == null) {
		page = "Home";
	}
	
	FetchData(page, null, null, "SELECT * FROM page ORDER BY `Order` ASC", "menu");
	FetchContent(null, page);
}

//CREATE A COOKIE

function createCookie(value) 
{
	var date = new Date();
    date.setTime(date.getTime() + (1*24*60*60*1000));
	document.cookie = "page="+value+"; expires="+date.toUTCString()+"; path=/";
}

//READ A COOKIE

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

//CREATE NAVIGATION MAIN MENU

function CreateMenu(json, page) 
{
	//parse page data
	var data = JSON.parse(json);

	//loop through all pages
	for (var i = 0; i < data.length; i++) 
    {
		//set base parent
		var parent = document.getElementById("navigation");

		//get individual row
		var row = data[i];

		//stop the display of submenu items and the all page
		if (row.Parent == null && row.Name != "All") 
		{
			//create li tag
			var li = CreateMenuLi(row, parent, page);

			parent = li;

			//create A tag
			var a = CreateMenuA(row, parent, page);

			//fetch submenu data
			FetchData(page, a, li, "SELECT * FROM Page WHERE Parent = '"+row.Name+"' ORDER BY `Order` ASC", "submenu");
		}
	}
}

//CREATE NAVIGATION SUBMENU

function CreateSubmenu(json, a, li, page) 
{
	li.className = "dropdown";

	a.setAttribute("data-toggle", "dropdown");
	a.setAttribute("role", "button");
	a.setAttribute("aria-haspopup", "true");
	a.setAttribute("aria-expanded", "false");
	a.onclick = null;

	var span = document.createElement("span");
	span.className = "caret";
	a.appendChild(span);

	var ul = document.createElement("ul");
	ul.className = "dropdown-menu";
	li.appendChild(ul);

	//loop through json
	var data = JSON.parse(json);

	//loop through all pages
	for (var i = 0; i < data.length; i++) 
    {
		//set base parent
		var parent = ul;

		//get individual row
		var row = data[i];
		
		//create li tag
		var li = CreateMenuLi(row, parent, page);

		parent = li;

		//create A tag
		var a = CreateMenuA(row, parent, page);
		
	}
}

//CREATE MENU LI

function CreateMenuLi(row, parent, page) 
{
	//create menu li
	var li = document.createElement("li");
	li.id = row.Name;
	if (row.Name.toUpperCase() == page.toUpperCase()) 
	{
		li.className = "active";
	}
	parent.appendChild(li);

	return li;
}

//CREATE MENU A

function CreateMenuA(row, parent, page) 
{
	//create menu a
	var a = document.createElement("a");
	a.onclick= function(){ ChangePage(row.Name) };

	var text = document.createTextNode(row.Name.toUpperCase());
	a.appendChild(text);
	parent.appendChild(a);

	return a;
}

//ALLOW USER TO CHANGE PAGE

function ChangePage(page) 
{
	//create or modify cookie
	createCookie(page);

	//reset active link
	var element= document.getElementsByClassName("active");
	if (element.length > 0) 
	{
		element = element[0];
		element.removeAttribute("class");
	}

	element = document.getElementById(page);
	element.className = "active";

	//remake content
	element = document.getElementById("content");
	element.innerHTML = "";
	FetchContent(null, page);
}

//FETCH CONTENT

function FetchContent(parentname, page) 
{
	//send request to php to fetch rows where parentname = WHATEVER PARENT and page = WHATEVER PAGE -> RECEIVE IN ORDER OF ORDER
	var query;
	
	if (parentname == null) 
	{
		query = "SELECT * FROM content WHERE Parent is null AND (Page = '"+page+"' Or Page = 'All') ORDER BY `order` ASC";
	} 
	else 
	{
		query = "SELECT * FROM content WHERE Parent = '"+parentname+"' AND (Page = '"+page+"' Or Page = 'All') ORDER BY `order` ASC";
	}

	FetchData(page, parentname, null, query, "display");
}

//FETCH GENERAL DATA

function FetchData(page, parameter1, parameter2, query, action) 
{
	var dbParam = JSON.stringify(query);
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() 
	{
    	if (this.readyState == 4 && this.status == 200) 
		{
			//fetched json string
			var json = xhttp.responseText;

			if (json != "0 results[]") 
			{
				if (action == "display") 
				{
					//tell it to display fetched data
					DisplayData(json, parameter1);
				}
				else if (action == "menu") 
				{
					//tell it to create menu
					CreateMenu(json, page);
				}
				else if (action == "submenu") 
				{
					//tell it to create submenu
					CreateSubmenu(json, parameter1, parameter2, page);
				}
				else if (action == "link") 
				{
					parameter1.onclick= function(){ ChangePage(parameter2) };
				}
			}
			else if (action == "link") 
			{
				parameter1.href = parameter2;
			}
		}
	};
	xhttp.open("POST", "../functions/query.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
}

//DISPLAY DATA

function DisplayData(json, parentname)
{
	//converts data into readable form
	var data = JSON.parse(json);

	//set defaults
	var previoustype = "";
	var position = 1;
	var rowelement;

	//method that loops through each row, and create the elements based on the row type
	for (var i = 0; i < data.length; i++) 
    {
		//get one row

        var row = data[i];

		if (row.Type == "IMGSmall" || row.Type == "PanelSmall" || row.Type == "IMGMedium")
		{
			if (row.Type != previoustype || ((row.Type == "IMGSmall" || row.Type == "PanelSmall") && position > 3) || (row.Type == "IMGMedium" && position > 2))
			{
				position = 1;
				rowelement = CreateRow(parentname);
			}
		}

		//parent = row.Parent 

		//create type for that row entry
		TypeFilter(row, rowelement, position);

		//change position and previous type
		position++;
		previoustype = row.Type;
	}
}

//CREATE CONTENT ELEMENTS BASED ON TYPE

function TypeFilter(row, rowelement, position)
{
	var parent;

	//get base parent
	if (row.Parent == null) 
	{
		parent = document.getElementById("content");
	}
	else 
	{
		parent = document.getElementById(row.Parent);
	}

	//modify parent
	if (row.Type == "IMGSmall" || row.Type == "PanelSmall") 
	{
		parent = CreateContainer(rowelement, "col-sm-4");
	}
	else if (row.Type == "IMGMedium") 
	{
		parent = CreateContainer(rowelement, "col-sm-6");
	}
	
	if (row.Type == "PanelSmall") 
	{
		console.log(parent);
		if (position == 1 )
		{
			parent = CreateContainer(parent, "col-sm-10");
		}
		else if (position == 2) 
		{
			parent = CreateContainer(parent, "col-sm-10 col-sm-offset-1");
		}
		else if (position == 3) 
		{
			parent = CreateContainer(parent, "col-sm-10 col-sm-offset-2");
		}
	}

	//if row has link, create link and make that the new parent
	if (row.Link != null & row.Type != "Li") 
	{
		parent = CreateLink(row.Link, parent);
	}

	//PARENTS
	if (row.Type == "PanelBig" || row.Type == "PanelSmall") 
	{
		CreatePanel(parent, row.Content, row.Page, row.Name);
	}
	else if (row.Type == "Jumbotron") 
	{
		CreateJumbotron(row.Content, row.Page, row.Name);
	}
	else if (row.Type == "Ol" || row.Type == "Ul") 
	{
		CreateList(parent, row.Page, row.Name, row.Type);
	}
	//CHILDREN
	else if (row.Type == "IMGSmall" || row.Type == "IMGMedium" || row.Type == "IMGBig") 
	{
		CreateImage(parent, row.Content, row.Alt);
	}
	else if (row.Type == "P" || row.Type == "H3") 
	{
		CreateText(parent, row.Content, row.Type);
	}
	else if (row.Type == "Li") 
	{
		CreateLi(parent, row.Content, row.Link);
	}
}

//CREATE PARENTS (CONTAINERS)

function CreateJumbotron(content, page, id) 
{
	var parent = document.getElementById("content");
	
	//create jumbotron
	var jumbotron = document.createElement("div");
	jumbotron.className = "jumbotron";
	jumbotron.id = id;
	parent.appendChild(jumbotron);
	
	parent = jumbotron;
	
	//create h1
	var h1 = document.createElement("h1");
	var text = document.createTextNode(content);
	h1.appendChild(text);
	parent.appendChild(h1);
	
	//create children
	FetchContent(id, page);
}

function CreateRow(parentname) 
{
	var parent;
	if (parentname == null) 
	{
		parent = document.getElementById("content");
	}
	else 
	{
		parent = document.getElementById(parentname);
	}

	//create row
	var row = document.createElement("div");
	row.className = "row";
	parent.appendChild(row);

	//return row
	return row;
}

function CreateContainer(parent, type) 
{
	//create container
	var container = document.createElement("div");
	container.className = type;
	parent.appendChild(container);
	
	//return outer
	return container;
}

function CreatePanel(parent, content, page, id) 
{
	//create panel
	var panel = document.createElement("div");
	panel.className = "panel";
	parent.appendChild(panel);
	
	parent = panel;
	
	//create panel heading
	var panelheading = document.createElement("div");
	panelheading.className = "panel-heading";
	parent.appendChild(panelheading);
	
	CreateText(panelheading, content, "p");
	
	//create panel body
	var panelbody = document.createElement("div");
	panelbody.className = "panel-body";
	panelbody.id = id;
	parent.appendChild(panelbody);

	parent = panelbody;
	
	//create children
	FetchContent(id, page);
}

function CreateList(parent, page, id, type) 
{
	//create list
	var list = document.createElement(type);
	list.id = id;
	parent.appendChild(list);

	parent = list;

	FetchContent(id, page);
}

//CREATE CHILDREN

function CreateLi(parent, content, link) 
{
	//create li
	var li = document.createElement("li");
	parent.appendChild(li);

	parent = li;

	if (link != null) 
	{
		parent = CreateLink(link, parent);
	}

	CreateText(parent, content, "p");
}

function CreateText(parent, content, type) 
{
	//create text
	var element = document.createElement(type);
	var text = document.createTextNode(content);
	element.appendChild(text);
	parent.appendChild(element);
}

function CreateImage(parent, image, alt)
{
	//create image
	var img = document.createElement("img");
	img.src = image;
	img.alt = alt;
	parent.appendChild(img);
}

function CreateLink(link, parent)
{
	//create link
	var a = document.createElement("a");

	FetchData(null, a, link, "select * from page where Name = '" + link + "'", "link");

	parent.appendChild(a);
	
	parent = a;

	return parent;
}
