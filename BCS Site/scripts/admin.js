function getcontent(query) {
    var dbParam = JSON.stringify(query);
    var xhttp = new XMLHttpRequest();
    var txt = "";

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            
            myObj = JSON.parse(xhttp.responseText);
            for (x in myObj) {
                // add each thing in the returned json to a string
                txt += myObj[x].type + "<br>";
            }
            //  put string inside element id.
            document.getElementById("demo").innerHTML = txt;
        }
    };
    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
}

function showpagecontent() {

    var pagepick = $(document.getElementById("pagepicker")).find("option:selected").text();
    var namelist = [];
    var xhttp = new XMLHttpRequest();
    var txt = "";
    var sql = "SELECT * from content WHERE Page like '"+pagepick+"' AND Parent IS null ORDER BY `Order` ASC";
    var dbParam = JSON.stringify(sql);

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            if(xhttp.responseText != "0 results[]"){
                txt = "<h3>Page Objects<h3><br/><table class='table'><tr><th>Order</th><th>Name</th><th>Type</th><th>Edit<th></tr>";
                 myObj = JSON.parse(xhttp.responseText);
                    for (x in myObj) {
                        txt += "<tr><td>"+myObj[x].Order+"</td><td>"+myObj[x].Name+"</td><td>"+myObj[x].Type+"</td><td><button class='btn btn-primary' onclick='loadEditpage(&apos;"+myObj[x].Name+"&apos;)'>Edit</button></td></tr>";
                    }
                    txt += "</table>";
                    }else{
                        txt = "<h3>Sorry no results found</h3>";
                    }
                document.getElementById("content-display").innerHTML = txt;
        }
    };
    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
}

function showadmin() {
    var pagepick = $(document.getElementById("pagepicker")).find("option:selected").text();
    var xhttp = new XMLHttpRequest();
    var txt = "<tr><th>id</th><th>name</th><th>password</th></tr>";
    var sql = "SELECT * from admin";
    var dbParam = JSON.stringify(sql);

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
            
            myObj = JSON.parse(xhttp.responseText);
            for (x in myObj) {
                // add each thing in the returned json to a string
                txt += "<tr><td>"+myObj[x].Id+"</td><td>"+myObj[x].Name+"</td><td>"+myObj[x].Password+"</td></tr>";
            }
            //  put string inside element id.
            document.getElementById("admintable").innerHTML = txt;
    }
    };
    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
}

function loadAddPage() {
    var pagepick = $(document.getElementById("pageselect")).find("option:selected").text();
    var typepick = $(document.getElementById("typeselect")).find("option:selected").text();

    document.getElementById("nameField").innerHTML = "";
    document.getElementById("orderField").innerHTML = "";
    document.getElementById("contentField").innerHTML = "";
    document.getElementById("linkField").innerHTML = "";
    document.getElementById("altField").innerHTML = "";
    document.getElementById("parentField").innerHTML = "";

    document.getElementById("nameField").innerHTML = "<label for='nameBox'>Name:</label><input type='text' class='form-control' id='nameBox'>";
    document.getElementById("orderField").innerHTML = "<label for='orderBox'>Order:</label><input type='text' class='form-control' id='orderBox'>";
    document.getElementById("contentField").innerHTML = "<label for='contentBox'>Content:</label><input type='text' class='form-control' id='contentBox'>";
    document.getElementById("linkField").innerHTML = "<label for='orderBox'>Link:</label><input type='text' class='form-control' id='linkBox'>";
    document.getElementById("altField").innerHTML = "<label for='altBox'>Alt-Text:</label><input type='text' class='form-control' id='altBox'>";
    document.getElementById("parentField").innerHTML = "";
    
    document.getElementById("contentField").style.display = "";
    document.getElementById("linkField").style.display = "";
    document.getElementById("altField").style.display = "";
    document.getElementById("parentField").style.display = "";
    
    if(typepick !="H3" && typepick != "P" && typepick != "IMGBig" && typepick != "IMGSmall" && typepick != "IMGMedium" && typepick != "Li"){
        document.getElementById("linkField").style.display = "none";
    }
    
    if(typepick != "IMGBig" && typepick != "IMGSmall" && typepick != "IMGMedium"){
        document.getElementById("altField").style.display = "none";
    }

    if(typepick == "Jumbotron" || typepick == "PanelBig" || typepick == "PanelSmall"){
        document.getElementById("parentField").style.display = "none";
    }

    if(typepick == "Ol" || typepick == "Ul"){
        document.getElementById("contentField").style.display = "none";
    }

    
    document.getElementById("insertField").innerHTML = "<button class='btn btn-primary' id='insertButton' onclick='insertContent()'>Insert</button>";
    fillparentpicker(pagepick, null);
}


function insertContent() {
    var pagepick = "'"+$(document.getElementById("pageselect")).find("option:selected").text()+"'";
    var typepick = "'"+$(document.getElementById("typeselect")).find("option:selected").text()+"'";
    var parpick = "'"+$(document.getElementById("parentselect")).find("option:selected").text()+"'";
    
    if(document.getElementById("nameBox").value == null || document.getElementById("orderBox").value == null){
        window.alert("Please ensure you have specified a valid Name and Order");
    }else{
        var nameTxt = "'"+document.getElementById("nameBox").value+"'";
        var orderTxt = "'"+document.getElementById("orderBox").value+"'";
        
        if(document.getElementById("contentBox").value == null || document.getElementById("contentBox").value == ""){
            var conTxt = "null";
        }else{
            conTxt = "'"+document.getElementById("contentBox").value+"'";
        }
        if(document.getElementById("linkBox").value == null || document.getElementById("linkBox").value == ""){
            var linkTxt = "null";
        }else{
            linkTxt = "'"+document.getElementById("linkBox").value+"'";
        }
        if(document.getElementById("altBox").value == null || document.getElementById("altBox").value == ""){
            var altTxt = "null"; 
        }else{
            altTxt = "'"+document.getElementById("altBox").value+"'";
        }
        if(parpick == null || parpick == "" || parpick == "''" || parpick == "'None'"){
            var parTxt = "null"; 
            
        }else{
            
            parTxt = parpick;
        }
               

        var sql = "INSERT into content values ("+nameTxt+","+pagepick+","+orderTxt+","+typepick+","+conTxt+","+linkTxt+","+altTxt+","+parTxt+")";
        var dbParam = JSON.stringify(sql);
        var xhttp = new XMLHttpRequest();
        

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                
                window.alert(xhttp.responseText)
            }
        };
        xhttp.open("POST", "../functions/execute.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("x=" + dbParam);
        }
}

function UpdateContent(){  
    var pagepick = "'"+$(document.getElementById("pageselect")).find("option:selected").text()+"'";
    var typepick = "'"+$(document.getElementById("typeselect")).find("option:selected").text()+"'";
    var parpick = "'"+$(document.getElementById("parentselect")).find("option:selected").text()+"'";

    if(document.getElementById("namespace").value == null || document.getElementById("orderBox").value == null){
        window.alert("Please ensure you have specified a valid Name and Order");
    }else{
        var nameTxt = "'"+document.getElementById("namespace").value+"'";
        var orderTxt = "'"+document.getElementById("orderBox").value+"'";
        
        if(document.getElementById("contentBox").value == null || document.getElementById("contentBox").value == ""){
            var conTxt = "null";
        }else{
            conTxt = "'"+document.getElementById("contentBox").value+"'";
        }
        if(document.getElementById("linkBox").value == null || document.getElementById("linkBox").value == ""){
            var linkTxt = "null";
        }else{
            linkTxt = "'"+document.getElementById("linkBox").value+"'";
        }
        if(document.getElementById("altBox").value == null || document.getElementById("altBox").value == ""){
            var altTxt = "null"; 
        }else{
            altTxt = "'"+document.getElementById("altBox").value+"'";
        }
        if(parpick == null || parpick == "" || parpick == "''" || parpick == "'None'"){
            
            var parTxt = "null"; 
        }else{
            
            parTxt = parpick;
        }
        

        var sql = "UPDATE content SET Page="+pagepick+", `Order`="+orderTxt+", Type="+typepick+", Content="+conTxt+", Link="+linkTxt+", Alt="+altTxt+", Parent="+parTxt+" WHERE Name="+nameTxt+"";
        var dbParam = JSON.stringify(sql);
        var xhttp = new XMLHttpRequest();
        

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                
                window.alert(xhttp.responseText)
            }
        };
        xhttp.open("POST", "../functions/execute.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("x=" + dbParam);
        
        }
}
function loadEditpage(name){
    createCookie('name', name, 1);
    eraseCookie('parent');
    window.location.href="edit.php";
}

function loadSubEditpage(name, parent){
    createCookie('name', name, 1);
    createCookie('parent', parent, 1);
    window.location.href="edit.php";
    }


function fetchdata(){
    var editname = readCookie('name');
    document.getElementById("nametitle").innerHTML = "<h1>BCS Admin Edit:"+editname+"</h1>";
    
    var sql = "SELECT * FROM content WHERE Name like '"+editname+"'";
    var dbParam = JSON.stringify(sql);
    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            myObj = JSON.parse(xhttp.responseText);

            fillEditPage(xhttp.responseText);
            }
            };

    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);


}

function fillEditPage(jstring) {
    myObj = JSON.parse(jstring);
    
    var name = myObj[0].Name;
    var page = myObj[0].Page;
    var order = myObj[0].Order;
    var type = myObj[0].Type;
    var content = myObj[0].Content;
    var link = myObj[0].Link;
    var alt = myObj[0].Alt;
    var parent = myObj[0].Parent;
    document.getElementById("namespace").value = name;
    document.getElementById("namespace").style.display = "none";
    document.getElementById("pageselect").value = page;
    document.getElementById("typeselect").value = type;
    document.getElementById("orderField").innerHTML = "";
    document.getElementById("contentField").innerHTML = "";
    document.getElementById("linkField").innerHTML = "";
    document.getElementById("altField").innerHTML = "";
    document.getElementById("parentField").innerHTML = "";
    document.getElementById("nameField").innerHTML = "<h3>Object Name: "+name+"</h3>";
    document.getElementById("orderField").innerHTML = "<label for='orderBox'>Order:</label><input type='text' class='form-control' id='orderBox'>";
    document.getElementById("contentField").innerHTML = "<label for='contentBox'>Content:</label><input type='text' class='form-control' id='contentBox'>";
    document.getElementById("linkField").innerHTML = "<label for='orderBox'>Link:</label><input type='text' class='form-control' id='linkBox'>";
    document.getElementById("altField").innerHTML = "<label for='altBox'>Alt-Text:</label><input type='text' class='form-control' id='altBox'>";
    document.getElementById("parentField").innerHTML = "";
    document.getElementById("orderBox").value = order;
    document.getElementById("contentBox").value = content;
    document.getElementById("linkBox").value = link;
    document.getElementById("altBox").value = alt;

    document.getElementById("linkField").style.display = "";
    document.getElementById("altField").style.display = "";
    document.getElementById("parentField").style.display = "";
    
   if(type !="H3" && type != "P" && type != "IMGBig" && type != "IMGSmall" && type != "IMGMedium" && type != "Li"){
        document.getElementById("linkField").style.display = "none";
    }
    
    if(type != "IMGBig" && type != "IMGSmall" && type != "IMGMedium"){
        document.getElementById("altField").style.display = "none";
    }

    if(type == "Jumbotron" || type == "PanelBig" || type == "PanelSmall"){
        document.getElementById("parentField").style.display = "none";
    }

    if(type == "Ol" || type == "Ul"){
        document.getElementById("contentField").style.display = "none";
    }
    document.getElementById("insertField").innerHTML = "<div class='col-sm-1 col-sm-offset-1'><button class='btn btn-danger' onclick='deleteobject(&apos;"+name+"&apos;)'>DELETE</buton></div><div class='col-sm-1 col-sm-offset-8'><button class='btn btn-primary' id='insertButton' onclick='UpdateContent(&apos;"+name+"&apos;)'>Update</button></div>";
    fillparentpicker(page, parent);
    showChildContent(name);
    

}



function fillparentpicker(page, parent){
    
    var txt = "<label for='parentselect'>Parent:</label><select name='Parent' class='form-control selectpicker' id='parentselect' Required><option value='None'>None</option>";
    var sql = "SELECT Name from content WHERE (Type like 'Jumbotron' OR Type like 'PanelBig' OR Type like 'PanelSmall' OR Type like 'Ol' OR Type like 'Ul') AND Page like '"+page+"'";
    var dbParam = JSON.stringify(sql);
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() 
    {
        if (xhttp.readyState == 4 && xhttp.status == 200) 
        {

            json = xhttp.responseText;
            if (json != "0 results[]") 
			{
                myObj = JSON.parse(json);
                for (x in myObj) 
                {
                    // add each thing in the returned json to a string
                    txt += "<option value='"+myObj[x].Name+"'>"+myObj[x].Name+"</option>";
                }
                //  put string inside element id.
                txt += "</select>"
                document.getElementById("parentField").innerHTML = txt;
                if (parent != null) 
                {
                    document.getElementById("parentselect").value = parent;
                }
            }
            else 
            {
                document.getElementById("parentField").innerHTML = "";
            }
        }
    };
    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
    
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function showChildContent(name) {

    var pagepick = $(document.getElementById("pagepicker")).find("option:selected").text();
    
    var xhttp = new XMLHttpRequest();
    var txt = "";
    var sql = "SELECT * from content WHERE Parent like '"+name+"' ORDER BY `Order` ASC";
    var dbParam = JSON.stringify(sql);

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            if(xhttp.responseText != "0 results[]"){
                txt = "<h3>SubObjects<h3><br/><table class='table'><tr><th>Order</th><th>Name</th><th>Type</th><th>Content</th><th>Edit<th></tr>";
                 myObj = JSON.parse(xhttp.responseText);
                    for (x in myObj) {
                        txt += "<tr><td>"+myObj[x].Order+"</td><td>"+myObj[x].Name+"</td><td>"+myObj[x].Type+"</td><td>"+myObj[x].Content+"</td><td><button class='btn btn-primary' onclick='loadSubEditpage(&apos;"+myObj[x].Name+"&apos;,&apos;"+name+"&apos;)'>Edit</button></td></tr>";
                    }
                    txt += "</table>";
                    }else{
                        txt = "<h3>Sorry no results found</h3>";
                    }
                document.getElementById("content-display").innerHTML = txt;
        }
    };

    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
    
}

function backbutton(){
    var Cookiemonster = readCookie('parent');

    if(Cookiemonster == null){
        window.location.href="adminhome.php";
    }else {
        
        loadEditpage(readCookie('parent'));
    }
}

function getName() 
{
    var page = $(document.getElementById("pageselect")).find("option:selected").text();
    var parent = $(document.getElementById("parentselect")).find("option:selected").text();
    var name = readCookie('name');
    refreshEdit(name);
    fillparentpicker(page, parent);
}

function refreshEdit(name){
    
    var pagepick = $(document.getElementById("pageselect")).find("option:selected").text();
    var typepick = $(document.getElementById("typeselect")).find("option:selected").text();
 
    document.getElementById("contentField").style.display = "";
    document.getElementById("linkField").style.display = "";
    document.getElementById("altField").style.display = "";
    document.getElementById("parentField").style.display = "";
    
    if(typepick !="H3" && typepick != "P" && typepick != "IMGBig" && typepick != "IMGSmall" && typepick != "IMGMedium" && typepick != "Li"){
        document.getElementById("linkField").style.display = "none";
    }
    
    if(typepick != "IMGBig" && typepick != "IMGSmall" && typepick != "IMGMedium"){
        document.getElementById("altField").style.display = "none";
    }

    if(typepick == "Jumbotron" || typepick == "PanelBig" || typepick == "PanelSmall"){
        document.getElementById("parentField").style.display = "none";
    }

    if(typepick == "Ol" || typepick == "Ul"){
        document.getElementById("contentField").style.display = "none";
    }

    showChildContent(name);

}

function CreatePage(){
    if(document.getElementById("nameBox").value == null || document.getElementById("nameBox").value == "" || document.getElementById("orderBox").value == null || document.getElementById("orderBox").value == ""){
        window.alert("Please ensure you have entered a Page name and Order");
    }
    else {
        var name = "'"+document.getElementById("nameBox").value+"'";
        var order = "'"+document.getElementById("orderBox").value+"'";
        var parpick = "'"+$(document.getElementById("parentselect")).find("option:selected").text()+"'";
        if(parpick == "'None'" || parpick == "''"){
            var parent = "null";
        }else{
            parent = parpick;
        }
    var sql = "INSERT into page values ("+name+","+order+","+parent+")";
    var dbParam = JSON.stringify(sql);
    var xhttp = new XMLHttpRequest();
        
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            window.alert(xhttp.responseText)
        }
    };
    xhttp.open("POST", "../functions/execute.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
    }
}

function UpdatePage(){
    var namepick = "'"+$(document.getElementById("pageselect")).find("option:selected").text()+"'";
    if(namepick == null || namepick == "''" || document.getElementById("orderBox").value == null || document.getElementById("orderBox").value == ""){
        window.alert("Please ensure you have entered a Page name and Order");
    }
    else {
        var name = namepick;
        var order = "'"+document.getElementById("orderBox").value+"'";
        var parpick = "'"+$(document.getElementById("parentselect")).find("option:selected").text()+"'";
        
        if(parpick == "'None'" || parpick == "''"){
            var parent = "null";
        }else{
            parent = parpick;
        }
    var sql = "UPDATE page SET `Order`="+order+",Parent="+parent+" WHERE Name="+name+"";
    var dbParam = JSON.stringify(sql);
    var xhttp = new XMLHttpRequest();
        
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            window.alert(xhttp.responseText)
        }
    };
    xhttp.open("POST", "../functions/execute.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
    }
}

function DeletePage(){
    var namepick = "'"+$(document.getElementById("pageselect")).find("option:selected").text()+"'";
    var r = confirm("Are you sure you want to delete Page "+namepick+"?");
    if (r == true) {
        
        sql = "DELETE FROM page WHERE Name="+namepick;
        var dbParam = JSON.stringify(sql);
        var xhttp = new XMLHttpRequest();
            
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                window.alert(xhttp.responseText)
                backbutton();
            }
        };
        xhttp.open("POST", "../functions/execute.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("x=" + dbParam);
    } 
}
    
function deleteobject(name){
    var r = confirm("Are you sure you want to delete "+name+"?");
    if (r == true) {
        sql = "DELETE FROM content WHERE Name='"+name+"'";
        var dbParam = JSON.stringify(sql);
        var xhttp = new XMLHttpRequest();
            
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                window.alert(xhttp.responseText)
                backbutton();
            }
        };
        xhttp.open("POST", "../functions/execute.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("x=" + dbParam);
    } 
}

function AdminLogin(){
    var user = "'"+document.getElementById("user").value+"'";
    var pass = "'"+document.getElementById("pass").value+"'";
    console.log(user);
    console.log(pass);
    var sql = "Select * from admin WHERE Name like "+user+" AND Password like "+pass;
    
    var dbParam = JSON.stringify(sql);
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log(xhttp.responseText);
            result = xhttp.responseText;
            
            if(result != "0 results[]"){
                createCookie('Login','true',1);
                window.alert('Welcome User');
                window.location.href ="adminhome.php";
                }else{
                    createCookie('Login', 'false', 1);
                    window.alert('Invalid user');
                }
            
        }
    };
    xhttp.open("POST", "../functions/query.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("x=" + dbParam);
        
}

function logincheck(){
    var cookie = readCookie("Login");
    if (cookie != "true"){
        window.location.href="index.php";
    }
}

function logout(){
    eraseCookie("Login");
    window.alert("Logout Successful");
    window.location.href="index.php";
}