<?php
include_once('creds.php');
date_default_timezone_set("Pacific/Auckland");

$sql = json_decode($_POST["x"], false);

$content = array();

$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $content[] = $row;
    }
} else {
    echo "0 results";
}
    header('Content-type: application/json');
    echo json_encode($content);



$conn->close();

?>