<?php
include_once('creds.php');
date_default_timezone_set("Pacific/Auckland");

$sql = json_decode($_POST["x"], false);
$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
$reply = "";
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
if ($conn->query($sql) === TRUE) {
    $reply = "Changes created successfully";
} else {
    $reply ="Error: " . $sql . "<br>" . $conn->error;
}

echo json_encode($reply);
$conn->close();

?>