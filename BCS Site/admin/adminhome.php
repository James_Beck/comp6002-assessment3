<?php   include_once('../functions/functions.php');
        session_start();
        //logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS ADMIN</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/admin.css" type="text/css" >
        <!--<link rel="stylesheet" href="../css/customstyles.css" type="text/css" >-->
    </head>
    <body class="backing" onload="logincheck()">
    <div class="container">
        <div class="row">
            <div class="page-header">
            <h1>BCS Admin</h1>
            </div>
        </div>
        <div class="row spacer">
            <div class="col-sm-3">
                <button class="btn btn-danger" onclick="logout()">Logout</button>
            </div>
            <div class="col-sm-3">
                <a href="pageadd.php" class="btn btn-primary">Add New Page</a>
            </div>
            <div class="col-sm-3">
                <a href="add.php" class="btn btn-primary">Add New Content</a>
            </div>
            <div class="col-sm-3">
                <a href="pageedit.php" class="btn btn-primary">Manage Pages</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <select name= "Page" class="form-control selectpicker" id="pagepicker" Required>";
                    <option value="pagename">--Please Select Page--</option>
                    <?PHP pageselect();?>
                </select>
            </div>
            <div class="col-sm-4">
                <button class="btn btn-primary" onclick="showpagecontent()">Load Page Info</button>
            </div>
        </div>
        <div class="row" id="content-display">
        </div>
    </div>
        <!-- Content ends here -->
    <script src="../scripts/admin.js"></script>
    
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
