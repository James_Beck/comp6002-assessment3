<?php   include_once('../functions/functions.php');
        session_start();
        //logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS ADMIN</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/admin.css" type="text/css" >
        <!--<link rel="stylesheet" href="../css/customstyles.css" type="text/css" >-->
    </head>
    <body class="backing" onload="logincheck()">
    <div class="container">
        <div class="row">
            <header class="page-header">
                    <h1>BCS Admin Add Content</h1>
                </header>
        </div>
        <div class="row spacer">
            <div class="col-sm-4"><a href="adminhome.php" class='btn btn-primary'>Back</a></div>
        </div>
        <div class ="row spacer">
            <div class="col-sm-6  col-sm-offset-3">
                <select name= "Page" class="form-control selectpicker" id="pageselect" onchange="loadAddPage()" Required>";
                    <option value="pagename">--Please Select Page--</option>
                    <?PHP pageselect();?>
                </select>
            </div>        
        </div>
        <div class="row spacer">
            <div class="col-sm-6  col-sm-offset-3">
                <select name="Type" class="form-control selectpicker" id="typeselect" onchange="loadAddPage()" Required>";
                    <option value="$pagename">--Please Select Type--</option>
                    <?PHP typeselect();?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="nameField"></div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="orderField"></div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="headingField"></div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="contentField"></div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="parentField"></div>
        </div>
        <div class="row">
            <div class="col-sm-10  col-sm-offset-1" id="linkField"></div>
        </div>
        <div class="row spacer">
            <div class="col-sm-10  col-sm-offset-1" id="altField"></div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-10" id="insertField">
            </div>
        </div>
        <div class="row" id ="box">
        </div>
    </div>
        <!-- Content ends here -->
    <script src="../scripts/admin.js"></script>
    
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
